FROM 		ubuntu:13.10
MAINTAINER	Alateas <dmitrymashkin@gmail.com>
RUN 		apt-get update
RUN 		apt-get install -y git
RUN 		apt-get install -y python python-dev python-distribute python-pip
ADD			requirements.txt /tmp/requirements.txt
RUN 		pip install -r /tmp/requirements.txt
EXPOSE 		80
CMD 		["python", "/vocavoca/src/main.py"]