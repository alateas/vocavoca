#!/bin/bash
SCRIPT_HOME="$( cd "$( dirname "$0" )" && pwd )"
cd $SCRIPT_HOME

case "$1" in
	start)
		sudo docker rm vocavoca > /dev/null 2>&1
		sudo docker run -name vocavoca -i -t -p 8000:80 -v $SCRIPT_HOME:/vocavoca vocavoca
		;;
	rebuild)
		git pull
		sudo docker build -t vocavoca .
		;;
esac
